<?php

/**
 * db_task query file.
 */      
function db_task_db_table_data_select($filter, $header) {
  $q = db_select('views_count', 'n')
    ->extend('PagerDefault')
    ->limit(5)
    ->extend('TableSort')
    ->orderByHeader($header);
  $q->join('users', 'u', 'n.uid = u.uid');
  $q->join('node', 'no', 'n.nid = no.nid');
  if ($filter > -1) {
    $q->condition('n.uid', $filter, '=');
  }
  $q->fields('n', array('nid', 'uid', 'v_count', 'viewed'));
  $q->fields('u', array('name'));
  $q->fields('no', array('title'));

  return $q->execute()->fetchAll();
}

function db_task_db_usernames_select() {
  $q = db_select('users', 'u');
  $q->fields('u', array('uid', 'name'));
  return $q->execute()->fetchAll();
}

function db_task_db_row_select($nid, $uid) {
  return db_select('views_count', 'n')
    ->fields('n', array('nid', 'uid', 'v_count', 'viewed'))
    ->condition('n.nid', $nid)
    ->condition('n.uid', $uid)
    ->execute()
    ->fetchAll();
}

function db_task_db_initial_row_insert($nid, $uid) {
  return db_insert('views_count')
    ->fields(array(
      'nid' => $nid,
      'uid' => $uid,
      'v_count' => '1',
      'viewed' => time(),
    ))
    ->execute();
}

function db_task_db_row_update($nid, $uid, $count) {
  $q = db_update('views_count');
  $q->fields(array('v_count' => $count, 'viewed' => time()));
  $q->condition('nid', $nid);
  $q->condition('uid', $uid);
  $q->execute();
}
