<?php

/**
 * db_task config form.
 */
function db_task_config_form() {
  $form = array();

  $users = db_task_db_usernames_select();

  $form['db_task_uid'] = array(
    '#type' => 'select',
    '#title' => t('User'),
    '#default_value' => variable_get('db_task_uid', '-1'),
  );
  $form['db_task_uid']['#options'][-1] = 'all';

  foreach ($users as $user) {
    if ($user->uid != 0) {
      $form['db_task_uid']['#options'][$user->uid] = $user->name;
    }
  }

  return system_settings_form($form);
}

function db_task_config_form_validate($form, &$form_state) {
  $uid = $form_state['values']['db_task_uid'];
  if (!is_numeric($uid)) {
    form_set_error('onthisdate_maxdisp', t('You must enter an positive integer for uid'));
  }
  elseif ($uid < -1) {
    form_set_error('onthisdate_maxdisp', t('uid must be positive.'));
  }
}

function db_task_make_views_table($filter) {

  $header = array(
    array('data' => 'node title', 'field' => 'title'),
    array('data' => 'user name', 'field' => 'name'),
    array('data' => 'view count', 'field' => 'v_count'),
    array('data' => 'node id', 'field' => 'nid'),
    array('data' => 'last view', 'field' => 'viewed'),
  );

  $nodes = db_task_db_table_data_select($filter, $header);

  $rows = array();
  foreach ($nodes as $node) {
    $name = ($node->name == '') ? 'anonymous' : $node->name;
    $rows[] = array(
      l($node->title, 'node/' . $node->nid),
      l($name, 'user/' . $node->uid),
      $node->v_count,
      $node->nid,
      date(DATE_RFC2822, $node->viewed)
    );
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= theme('pager');

  return $output;
}