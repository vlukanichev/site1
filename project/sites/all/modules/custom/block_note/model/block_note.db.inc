<?php

function block_note_db_select($delta) {
  return db_select('block_notes', 'b')
          ->fields('b', array('delta', 'note'))
          ->condition('b.delta', $delta)
          ->execute()
          ->fetchAll();
}

function block_note_db_insert($delta, $note) {
  $db_name = 'block_notes';
  $data = array(
    'delta' => $delta,
    'note' => $note,
  );
  if (block_note_db_select($delta) != NULL) {
    drupal_write_record($db_name, $data, 'delta');
  }
  else {
    drupal_write_record($db_name, $data);
  }
}

function block_note_db_delete($delta) {
  db_delete('block_notes')
      ->condition('delta', $delta)
      ->execute();
}
