<?php

/**
 * Implements hook_init().
 */
function db_task_init() {
    module_load_include('inc', 'db_task', 'includes/db_task.pages');
    module_load_include('inc', 'db_task', 'includes/db_task.admin');
    module_load_include('inc', 'db_task', 'model/db_task.db');
}

/**
 * Implements hook_schema().
 */
function db_task_menu() {
    $items = array();
    $items['views_count'] = array(
        'title' => 'views count table',
        'page callback' => 'db_task_main_function',
        'access callback' => TRUE,
    );

    $items['admin/config/db_task_config'] = array(
        'title' => 'node views page',
        'description' => 'Description',
        'page callback' => 'db_task_table_page',
        'access arguments' => array('administer site configuration'),
    );
    return $items;
}

/**
 * Implements hook_view().
 */
function db_task_node_view($node, $view_mode, $langcode) {

    global $user;
    $nid = $node->nid;
    $uid = $user->uid;
    if ($uid == 0) {
        return;
    }
    $res = db_task_db_row_select($nid, $uid);
    if ($res == NULL) {
        db_task_db_initial_row_insert1($nid, $uid);
    }
    else {
        dsm($res[0]->viewed);
        if (intval($res[0]->viewed) + 120 < time()) {
            $count = 1 + intval($res[0]->v_count);
            db_task_db_row_update($nid, $uid, $count);
        }
    }
}

function db_task_main_function() {

    return 'page';
}

/**
 * db_task config form.
 */
function db_task_config_form() {
    $form = array();

    $users = db_task_db_usernames_select();

    $form['db_task_uid'] = array(
        '#type' => 'select',
        '#title' => t('User'),
        '#default_value' => variable_get('db_task_uid', '-1'),
    );
    $form['db_task_uid']['#options'][-1] = 'all';

    foreach ($users as $user) {
        if ($user->uid != 0) {
            $form['db_task_uid']['#options'][$user->uid] = $user->name;
        }
    }

    return system_settings_form($form);
}

function db_task_config_form_validate($form, &$form_state) {
    $uid = $form_state['values']['db_task_uid'];
    if (!is_numeric($uid)) {
        form_set_error('onthisdate_maxdisp', t('You must enter an positive integer for uid'));
    }
    elseif ($uid < -1) {
        form_set_error('onthisdate_maxdisp', t('uid must be positive.'));
    }
}

function db_task_make_views_table($filter) {

    $header = array(
        array('data' => 'node title', 'field' => 'title'),
        array('data' => 'user name', 'field' => 'name'),
        array('data' => 'view count', 'field' => 'v_count'),
        array('data' => 'node id', 'field' => 'nid'),
        array('data' => 'last view', 'field' => 'viewed'),
    );

    $nodes = db_task_db_table_data_select($filter, $header);

    $rows = array();
    foreach ($nodes as $node) {
        $name = ($node->name == '') ? 'anonymous' : $node->name;
        $rows[] = array(
            l($node->title, 'node/' . $node->nid),
            l($name, 'user/' . $node->uid),
            $node->v_count,
            $node->nid,
            date(DATE_RFC2822, $node->viewed)
        );
    }

    $output = theme('table', array('header' => $header, 'rows' => $rows));
    $output .= theme('pager');

    return $output;
}
/**
 * db_task query file.
 */
function db_task_db_table_data_select($filter, $header) {
    $q = db_select('views_count', 'n')
        ->extend('PagerDefault')
        ->limit(5)
        ->extend('TableSort')
        ->orderByHeader($header);
    $q->join('users', 'u', 'n.uid = u.uid');
    $q->join('node', 'no', 'n.nid = no.nid');
    if ($filter > -1) {
        $q->condition('n.uid', $filter, '=');
    }
    $q->fields('n', array('nid', 'uid', 'v_count', 'viewed'));
    $q->fields('u', array('name'));
    $q->fields('no', array('title'));

    return $q->execute()->fetchAll();
}

function db_task_db_usernames_select() {
    $q = db_select('users', 'u');
    $q->fields('u', array('uid', 'name'));
    return $q->execute()->fetchAll();
}

function db_task_db_row_select($nid, $uid) {
    return db_select('views_count', 'n')
        ->fields('n', array('nid', 'uid', 'v_count', 'viewed'))
        ->condition('n.nid', $nid)
        ->condition('n.uid', $uid)
        ->execute()
        ->fetchAll();
}

function db_task_db_initial_row_insert1($nid, $uid) {
    return db_insert('views_count')
        ->fields(array(
            'nid' => $nid,
            'uid' => $uid,
            'v_count' => '1',
            'viewed' => time(),
        ))
        ->execute();
}

function db_task_db_row_update($nid, $uid, $count) {
    $q = db_update('views_count');
    $q->fields(array('v_count' => $count, 'viewed' => time()));
    $q->condition('nid', $nid);
    $q->condition('uid', $uid);;
    $q->execute();
}
