<?php

function new_profile_name_show_form($form, &$form_state) {
  global $user;
  $form = array();
  $uid = intval($_SESSION['uid']);
  $p = entity_metadata_wrapper('profile2', $uid);

  $form['#prefix'] = '<div id="name-edit-form-id">';
  $form['#suffix'] = '</div>';

  $form['new_profile_profile_name_mark'] = array(
    '#type' => 'markup',
    '#markup' => $p->field_first_name->value(),
    '#prefix' => '<b>Name: </b><span id="name-mark">',
    '#suffix' => '</span>',
  );

  if (in_array('administrator', array_values($user->roles)) ||
      (in_array('ownprofileeditor', array_values($user->roles)) && $user->uid == $uid)) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Edit'),
      '#prefix' => '<span id="name-submit-edit">',
      '#suffix' => '</span>',
      '#ajax' => array(
        'callback' => 'new_profile_name_show_form_submit',
        'wrapper' => 'name-edit-form-id',
        'effect' => 'fade',
        'method' => 'replace',
      ),
    );
  }
  return $form;
}

function new_profile_name_show_form_submit(&$form, &$form_state) {
  return drupal_get_form('new_profile_name_edit_form');
}

function new_profile_name_edit_form($form, &$form_state) {
  $form = array();
  $uid = intval($_SESSION['uid']);
  $p = entity_metadata_wrapper('profile2', $uid);

  $form['#prefix'] = '<div id="name-edit-form-id">';
  $form['#suffix'] = '</div>';

  $form['new_profile_profile_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $p->field_first_name->value(),
    '#prefix' => '<span id="name-edit">',
    '#suffix' => '</span>',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#prefix' => '<span id="name-submit-save">',
    '#suffix' => '</span>',
    '#ajax' => array(
      'callback' => 'new_profile_name_edit_form_submit',
      'wrapper' => 'name-edit-form-id',
      'effect' => 'fade',
      'method' => 'replace',
    ),
  );

  return $form;
}

function new_profile_name_edit_form_submit(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  drupal_validate_form('new_profile_name_edit_form', $form, $form_state);
  if (form_get_errors()) {
    return $form;
  }
  $uid = intval($_SESSION['uid']);
  $name = $form_state['values']['new_profile_profile_name'];
  $p = entity_metadata_wrapper('profile2', $uid);
  $p->field_first_name = $name;
  $p->save();
  return drupal_get_form('new_profile_name_show_form');
}

function new_profile_surname_show_form($form, &$form_state) {
  global $user;

  $form = array();
  $uid = intval($_SESSION['uid']);
  $p = entity_metadata_wrapper('profile2', $uid);

  $form['#prefix'] = '<div id="surname-edit-form-id">';
  $form['#suffix'] = '</div>';

  $form['new_profile_profile_surname_mark'] = array(
    '#type' => 'markup',
    '#markup' => $p->field_surname->value(),
    '#prefix' => '<b>Surname: </b><span id="surname-mark">',
    '#suffix' => '</span>',
  );

  if (in_array('administrator', array_values($user->roles)) ||
      (in_array('ownprofileeditor', array_values($user->roles)) && $user->uid == $uid)) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Edit'),
      '#prefix' => '<span id="surname-submit-edit">',
      '#suffix' => '</span>',
      '#ajax' => array(
        'callback' => 'new_profile_surname_show_form_submit',
        'wrapper' => 'surname-edit-form-id',
        'effect' => 'fade',
        'method' => 'replace',
      ),
    );
  }

  return $form;
}

function new_profile_surname_show_form_submit(&$form, &$form_state) {
  return drupal_get_form('new_profile_surname_edit_form');
}

function new_profile_surname_edit_form($form, &$form_state) {
  $form = array();
  $uid = intval($_SESSION['uid']);
  $p = entity_metadata_wrapper('profile2', $uid);

  $form['#prefix'] = '<div id="surname-edit-form-id">';
  $form['#suffix'] = '</div>';

  $form['new_profile_profile_surname'] = array(
    '#type' => 'textfield',
    '#title' => t('Surname'),
    '#default_value' => $p->field_surname->value(),
    '#prefix' => '<span id="surname-edit">',
    '#suffix' => '</span>',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#prefix' => '<span id="surname-submit-save">',
    '#suffix' => '</span>',
    '#ajax' => array(
      'callback' => 'new_profile_surname_edit_form_submit',
      'wrapper' => 'surname-edit-form-id',
      'effect' => 'fade',
      'method' => 'replace',
    ),
  );

  return $form;
}

function new_profile_surname_edit_form_submit(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  drupal_validate_form('new_profile_surname_edit_form', $form, $form_state);
  if (form_get_errors()) {
    return $form;
  }
  $uid = intval($_SESSION['uid']);
  $surname = $form_state['values']['new_profile_profile_surname'];
  $p = entity_metadata_wrapper('profile2', $uid);
  $p->field_surname = $surname;
  $p->save();
  return drupal_get_form('new_profile_surname_show_form');
}


function new_profile_dob_show_form($form, &$form_state) {
  global $user;
  $form = array();
  $uid = intval($_SESSION['uid']);
  $p = entity_metadata_wrapper('profile2', $uid);

  $form['#prefix'] = '<div id="dob-edit-form-id">';
  $form['#suffix'] = '</div>';

  $form['new_profile_profile_dob_mark'] = array(
    '#type' => 'markup',
    '#markup' => date('l jS \of F Y', $p->field_dob->value()),
    '#prefix' => '<b>DOB: </b><span id="dob-mark">',
    '#suffix' => '</span>',
  );
  if (in_array('administrator', array_values($user->roles)) ||
      (in_array('ownprofileeditor', array_values($user->roles)) && $user->uid == $uid)) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Edit'),
      '#prefix' => '<span id="dob-submit-edit">',
      '#suffix' => '</span>',
      '#ajax' => array(
        'callback' => 'new_profile_dob_show_form_submit',
        'wrapper' => 'dob-edit-form-id',
        'effect' => 'fade',
        'method' => 'replace',
      ),
    );
  }

  return $form;
}

function new_profile_dob_show_form_submit(&$form, &$form_state) {
  return drupal_get_form('new_profile_dob_edit_form');
}

function new_profile_dob_edit_form($form, &$form_state) {
  $form = array();
  $format = 'Y-m-d';
  $uid = intval($_SESSION['uid']);
  $p = entity_metadata_wrapper('profile2', $uid);

  $form['#prefix'] = '<div id="dob-edit-form-id">';
  $form['#suffix'] = '</div>';

  $form['new_profile_profile_dob'] = array(
    '#type' => 'date_select',
    '#date_format' => $format,
    '#title' => t('DOB'),
    '#date_year_range' => '-90:+1',
    '#default_value' => date($format, $p->field_dob->value()),
    '#prefix' => '<span id="dob-edit">',
    '#suffix' => '</span>',
    '#required' => TRUE,
  );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#prefix' => '<span id="dob-submit-save">',
      '#suffix' => '</span>',
      '#ajax' => array(
        'callback' => 'new_profile_dob_edit_form_submit',
        'wrapper' => 'dob-edit-form-id',
        'effect' => 'fade',
        'method' => 'replace',
      ),
    );

  return $form;
}

function new_profile_dob_edit_form_submit(&$form, &$form_state) {
  $form_state['rebuild'] = FALSE;
  drupal_validate_form('new_profile_profile_dob_form', $form, $form_state);
  if (form_get_errors()) {
    return $form;
  }
  $uid = intval($_SESSION['uid']);
  $dob = $form_state['values']['new_profile_profile_dob'];
  $p = entity_metadata_wrapper('profile2', $uid);
  $p->field_dob = strtotime($dob);
  $p->save();
  return drupal_get_form('new_profile_dob_show_form');
}
