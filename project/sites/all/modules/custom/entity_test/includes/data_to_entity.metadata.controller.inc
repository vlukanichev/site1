<?php

class MyEntityMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {

    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['viewer'] = array(
      'label' => t('Node viewer'),
      'type' => 'user',
      'description' => t("The viewer of the node."),
      'setter permission' => 'administer nodes',
      'required' => TRUE,
      'schema field' => 'uid',
    );

    $properties['node'] = array(
      'label' => t('Node'),
      'type' => 'node',
      'description' => t("Node info."),
      'setter permission' => 'administer nodes',
      'required' => TRUE,
      'schema field' => 'nid',
    );

    $properties['viewed'] = array(
      'label' => t('Last viewed'),
      'type' => 'date',
      'description' => t('Last viewed time'),
      'schema field' => 'viewed',
    );

    return $info;
  }

}
