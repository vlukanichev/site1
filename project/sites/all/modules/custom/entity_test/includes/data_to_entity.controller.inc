<?php

class DataToEntityController extends EntityAPIController {

  public function __construct($values = array()) {
    parent::__construct($values, 'data_to_entity_controller');
  }

  public function create(array $values = array()) {
    global $user;

    $values += array(
      'uid' => $user->uid,
      'nid' => 0,
      'v_count' => 1,
      'viewed' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $entity->viewed = REQUEST_TIME;
    return parent::save($entity, $transaction);
  }

}
