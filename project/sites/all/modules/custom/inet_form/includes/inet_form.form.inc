<?php

function inet_form_reg_form($form, &$form_state) {

  $form = array(
    '#prefix' => '<div id = "form-ajax-form">',
    '#suffix' => '</div>',
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Your name'),
    '#required' => TRUE,
  );

  $form['sex'] = array(
    '#type' => 'radios',
    '#title' => t('Sex'),
    '#options' => array(0 => t('Male'), 1 => t('Female')),
    '#required' => TRUE,
  );

  $form['age'] = array(
    '#type' => 'textfield',
    '#title' => t('Age'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#required' => TRUE,
  );

  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#required' => TRUE,
  );

  $form['text'] = array(
    '#type' => 'textarea',
    '#title' => t('Text of message'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#ajax' => array(
      'callback' => 'inet_form_foo_form_reload_form',
      'method' => 'replace',
      'effect' => 'fade',
      'wrapper' => 'form-ajax-form',
    ),
  );

  return $form;
}

function inet_form_foo_form_reload_form($form, &$form_state) {
  drupal_validate_form('inet_form_reg_form', $form, $form_state);

  if (form_get_errors()) {
    $form_state['rebuild'] = TRUE;
    return $form;
  }

  $form['text']['#value'] = '';
  $form['name']['#value'] = '';
  $form['sex']['#option'] = '';
  $form['age']['#value'] = '';
  $form['subject']['#value'] = '';

  $params = $form_state['values'];
  $body = t('Name: ') . $params['name'];
  $body .= '<br />' . t('Sex: ') . $params['sex'];
  $body .= '<br />' . t('Age: ') . $params['age'];
  $body .= '<br />' . t('Subject: ') . $params['subject'];
  $body .= '<br />' . t('Text: ') . $params['text'];

  $to = variable_get('site_mail', '');

  drupal_mail('system', 'mail', $to, language_default(), array(
    'context' => array(
      'subject' => 'Some subject',
      'message' => $body,
    )
  ));

  $form_state['rebuild'] = TRUE;
  drupal_set_message(t('Your message was send'));
  return $form;
}
