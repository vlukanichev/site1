<?php

/**
 * w_modal config form.
 */
function w_modal_config_form($form, &$form_state) {
  $form = array();

  $form['w_modal_hat_color'] = array(
    '#type' => 'textfield',
    '#title' => t('hat color'),
    '#default_value' => variable_get('w_modal_hat_color', '900'),
  );

  $form['w_modal_back_color'] = array(
    '#type' => 'textfield',
    '#title' => t('back color'),
    '#default_value' => variable_get('w_modal_back_color', '999'),
  );

  return ($form);
}

function w_modal_config_form_validate($form, &$form_state) {
  $hat = $form_state['values']['w_modal_hat_color'];
  if (!is_numeric($hat)) {
    form_set_error('onthisdate_maxdisp', t('You must enter an integer for hat color'));
  }
  elseif ($hat <= 0) {
    form_set_error('onthisdate_maxdisp', t('Color value must be positive.'));
  }
  $back = $form_state['values']['w_modal_back_color'];
  if (!is_numeric($back)) {
    form_set_error('onthisdate_maxdisp', t('You must enter an integer for back color'));
  }
  elseif ($back <= 0) {
    form_set_error('onthisdate_maxdisp', t('Color value must be positive.'));
  }
}
