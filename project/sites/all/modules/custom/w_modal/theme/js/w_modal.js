(function ($) {

  $(document).ready(function () {
    $('#page').append('<div id="modal">\n\
      <div id="hat">\n\
          <span id="modal_close">X</span>\n\
      </div>\n\
      </div><div id="overlay"></div>');
    var bColor = '#';
    bColor += Drupal.settings.w_modal.w_back_color;
    $('#modal').css('background-color', bColor);
    var hColor = '#';
    hColor += Drupal.settings.w_modal.w_hat_color;
    $('#hat').css('background-color', hColor);
    catchTheMessges();
    var mouseDown = 0;
    $('#hat').mousedown(function () {
      ++mouseDown;
    });
    $('#hat').mouseup(function () {
      --mouseDown;
    });
    $(document).mousemove(function (event) {
      if (mouseDown === 1) {
        mouseCoords(event);
      }
    });
    $('#go').click(function (event) {
      event.preventDefault();
      $('#overlay').fadeIn(400, function () {
          $('#modal')
            .css('display', 'block')
            .animate({opacity: 1, top: '50%'}, 200);
        });
    });

    $('#modal_close, #overlay').click(function () {
      $('#modal')
        .animate({opacity: 0, top: '45%'}, 200, function () {
            $(this).css('display', 'none');
            $('#overlay').fadeOut(400);
          }
        );
    });
  });

  function catchTheMessges() {
    var m = $('#messages');
    var msg = m.html();
    if (msg !== null) {
      $('#overlay').fadeIn(400, function () {
          $('#modal')
            .css('display', 'block')
            .animate({opacity: 1, top: '50%'}, 200);
        });
      $('#modal').append(msg);
      m.remove();
    }
  }
  
  function mouseCoords(e) {
    x = e.pageX;
    y = e.pageY;
    $('#modal').css('left', x - 5 + 'px');
    $('#modal').css('top', y - 5 + 'px');
  }

})(jQuery);