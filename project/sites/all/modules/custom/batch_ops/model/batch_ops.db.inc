<?php

function batch_ops_db_nids_select() {
  $q = db_select('node', 'n');
  $q->fields('n', array('nid'));
  $q->condition('type', 'currency');
  $array = $q->execute()->fetchAll();
  $res=array();
  foreach ($array as $value) {
    array_push($res, $value->nid);
  }
  return $res;
}
