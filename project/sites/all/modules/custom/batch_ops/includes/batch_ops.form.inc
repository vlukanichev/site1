<?php

/*
 * Batch start form
 */

function batch_ops_batch_form($form, &$form_state) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Начать',
  );
  return $form;
}

function batch_ops_batch_form_submit($form, &$form_state) {
  $nids = batch_ops_db_nids_select();
  $rates = batch_ops_make_data();
  batch_ops_do_work_batch($rates, $nids);
}

/*
 * Preparing data
 */

function batch_ops_make_data() {
  $acronims = array(
    'USD',
    'EUR',
    'MNT',
  );
  $rates = array(
    rand(1, 1000),
    rand(1, 1000),
    rand(1, 1000)
  );
  return batch_ops_make_rates_array($acronims, $rates);
}

function batch_ops_make_rates_array($acronims = array(), $rates = array()) {
  $res = array();
  $len = sizeof($rates);
  for ($i = 0; $i < $len; $i++) {
    $res['acronim'][$i] = $acronims[$i];
    $res['rate'][$i] = $rates[$i];
  }
  return $res;
}

/*
 * Update batch operation
 */

function batch_ops_do_work_batch($rates, $nids) {
  $operations = array();
  while ($nids) {
    $nids_part = array_splice($nids, 0, 1);
    $operations[] = array('batch_ops_set_rates', array($rates, $nids_part));
  }
  $batch = array(
    'title' => t('Updating rates'),
    'operations' => $operations,
  );
  batch_set($batch);
  batch_process();
}

function batch_ops_set_rates($rates = array(), $nids = array()) {
  for ($i = 0; $i < sizeof($rates['rate']); $i++) {
    foreach ($nids as $nid) {
      $node = node_load($nid);
      try {
        $wrapper = entity_metadata_wrapper('node', $node);
        $acronim = $wrapper->field_acronim->value();
      } catch (EntityMetadataWrapperException $exc) {
        batch_ops_log_err(__FUNCTION__, $exc);
      }
      if ($acronim == $rates['acronim'][$i]) {
        batch_ops_update_rates($node, $rates['rate'][$i], $wrapper);
      }
    }
  }
}

function batch_ops_update_rates($node, $rate, $wrapper) {
  $fc = batch_ops_get_today_rate($wrapper, $rate);
  if ($fc != NULL) {
    $fc->save();
  }
  else {
    $fc_val = entity_create('field_collection_item', array('field_name' => 'field_rate'));
    $fc_val->setHostEntity('node', $node);
    try {
      $q = entity_metadata_wrapper('field_collection_item', $fc_val);
    } catch (EntityMetadataWrapperException $exc) {
      batch_ops_log_err(__FUNCTION__, $exc);
    }
    $q->field_curr_rate = $rate;
    $q->field_date = time();
    $q->save();
  }
  $wrapper->field_last_update = time();
  $wrapper->save();
}

function batch_ops_get_today_rate($wrapper, $rate) {
  foreach ($wrapper->field_rate->value() as $v) {
    try {
      $q = entity_metadata_wrapper('field_collection_item', $v->item_id);
      $date = $q->field_date->value();
    } catch (EntityMetadataWrapperException $exc) {
      batch_ops_log_err(__FUNCTION__, $exc);
    }
    if (date('m/d/y/', $date) == date('m/d/y/')) {
      $q->field_curr_rate = $rate;
      return $q;
    }
  }
  return NULL;
}

function batch_ops_action(&$node, $context = array()) {
  $nids = array($node->nid);
  $rates = batch_ops_make_data();
  batch_ops_set_rates($rates, $nids);
}

function batch_ops_log_err($func_name, $err) {
  watchdog(
      'batch_ops', 'See ' . $func_name . '() <pre>' . $err->getTraceAsString() . '</pre>', NULL, WATCHDOG_ERROR
  );
}
